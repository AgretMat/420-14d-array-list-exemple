package com.dinfogarneau.tp.array_exemple;

public class Personne {
    private int age;
    private String nom;
    private String Prenom;

    public Personne(int age, String nom, String prenom) {
        this.age = age;
        this.nom = nom;
        Prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }
}
