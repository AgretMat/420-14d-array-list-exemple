package com.dinfogarneau.tp.array_exemple;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

//https://www.javatpoint.com/collections-in-java
//https://www.javatpoint.com/array-in-java

public class MainActivity extends AppCompatActivity {

    public String[] tab;
    public int[] intTab;

    public String[][] tabDouble;
    public List<Personne> personne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tab = new String[10];
        tab[5] = "AA";
        tab[7] = "BB";
        int length = tab.length;
        System.out.println("\nContenu de tab");
        for (int i = 0; i < tab.length; i++) {
            System.out.println(" " + tab[i]);
        }

        tabDouble = new String[][]{{"E", "D"}, {"F", "G"}};
        System.out.println("\nContenu de tabDouble");
        for (int i = 0; i < tabDouble.length; i++) {
            System.out.println(" " + tabDouble[i][0]);
            System.out.println(" " + tabDouble[i][1]);
        }

        String[] tableau = {"A", "B", "C", "D"};
        System.out.println("\nContenu du tableau");
        for (String str : tableau) {
            System.out.print(" " + str);
        }

        List<String> liste = new ArrayList<>();
        liste.add("EE");
        liste.add("FF");
        liste.add("GG");
        liste.add("HH");
        String index = liste.get(0);
//        liste.clear();
        liste.remove(0);

        System.out.println("\nContenu de la liste");
        for (int i = 0; i < liste.size(); i++) {
            System.out.println(liste.get(i));
        }

        System.out.println(" " + liste.get(0));
        for (String str : liste) {
            System.out.print(" " + str);
        }

        personne = new ArrayList<>();
        personne.add(new Personne(89, "Gilles", "Garneau"));
        personne.add(new Personne(45, "nom1", "prenom1"));

        System.out.println("\nContenu de personne");
        for (Personne person : personne) {
            System.out.println(person.getNom());
        }

    }
}